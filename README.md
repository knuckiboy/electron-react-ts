# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
    <p>Electron App with React and Typescript setup</p>
* Version
    <p>Currently running on React 18 and Typescript 5</p>

### How do I get set up? ###

* Follow scripts in package.json file to follow dev or build commands
* Run dev script will require to reload the page to refresh changes
* Run prod script to run on prod build
* Update env variable to change to run on port or on file
* Use electron builder to package build for mac or windows app
* Refresh/Upgrade dependencies with yarn outdated and check for minor or major upgrades before run yarn upgrade 

### Contribution guidelines ###

* Writing tests
* Project ideas
* Configurable setup
* Add electron package/ builder

