const { app, BrowserWindow } = require('electron');
const path = require("path");
require('dotenv').config({ path: path.resolve(__dirname, '../.env') })

const createWindow = () => {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            devTools: process.env.ENV == "DEV",
            nodeIntegration: true,
        }
    })

    if (process.env.ENV == "DEV") {
        win.loadURL('http://localhost:3000')
    } else {
        win.loadFile(`${path.join(__dirname, '../build/index.html')}`)
    }
}


app.on("ready", () => {
    createWindow()
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})