import React from "react";
import "./scss/index.scss";

const App: React.FC= () => {
  return (
    <div className="root-container">
      <h1>React 18 and TypeScript 5 App!🚀</h1>
    </div>
  );
};

App.propTypes = {};

export default App;
